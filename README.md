# Manufacturer Logistics Monitoring

This repository contains the code and source files to compare two event
processing platforms, INSTANS and Esper, in a manufacturer logistics
supply chain monitoring task.

## Supported platforms

Execution has been tested on Mac OS X. The code contained in this repository consists of straightforward bash
shell scripts, which could be executed on any platform supporting
bash. The status of INSTANS and Esper support for other platforms should be
checked from those respective repositories.

## Status

Experiments have been completed and the results are being proposed for
publication.

## Results

The results collected by authors can be found in the "results"-folder.

## Installation and execution

1. Install [INSTANS](https://github.com/aaltodsg/instans)
2. Install [XEvePro](https://github.com/aaltodsg/xevepro)
3. Set two environment variables:
  * `$INSTANS_HOME` pointing to the root directory of INSTANS
  * `$XEVEPRO_HOME` pointing to the root directory of XEvePro
4. Examples for execution can be found in the `scripts` directory:
  * `./batch_compare_output.sh` executes output comparison between INSTANS
and XEvePro on all test cases.
  * `./batch_compare_speed.sh` runs execution speed tests on all test
cases.
  * Simple command line examples to execute any individual test case on
both INSTANS and XEvePro can be found in the above shell scripts.

