#!/bin/bash

# Compare execution speed between Esper and INSTANS

function params () {
    echo "Two parameters required: 1 2"
    echo "1 = name of the parameter file without path, .epl or .ttl suffix"
    echo "2 = name of the event file without path, .trig or .xml suffix"
    echo "Apart from the suffixes, the file names in \"instans\" and \"esper\" directories are expected to match"
    exit 1
}

if [ "$#" -ne 2 ]; then
    params
fi

ORIGINALPATH=`pwd`
SDAT="$ORIGINALPATH/../data/EventRuns/"
IP="$ORIGINALPATH/../instans/"
EP="$ORIGINALPATH/../esper/"

# COMMENT OUT ONE OF THE FOLLOWING:
INSTANSQUERIES="-r \""$IP"time_engine.rq\" -r \""$IP"queries_comm_pack.rq\"" # Without shipping
# INSTANSQUERIES="-r \""$IP"time_engine.rq\" -r \""$IP"queries_comm_pack.rq\" -r \""$IP"queries_pack_ship.rq\"" # With shipping

# COMMENT OUT ONE OF THE FOLLOWING:
ESPERQUERIES="-r \""$EP"time_extract.epl\" -r \""$EP"queries_comm_pack.epl\""     # Without shipping
# ESPERQUERIES="-r \""$EP"time_extract.epl\" -r \""$EP"queries_comm_pack.epl\" -r \""$EP"queries_pack_ship.epl\""     # With shipping

cd "$XEVEPRO_HOME"

for i in `seq 1 4` ; do
  SBT_COMMAND='run --csv-output=/dev/null -r "'"$EP$1"'.epl" '"$ESPERQUERIES"' --time=- -i "'"$SDAT$2"'.xml"'
  ESPERTIME=$(sbt ''"$SBT_COMMAND"'' | grep Done | sed 's/[^0-9+\.0-9+]//g')
  Echo "Esper,$1,$2,$i,$ESPERTIME"
done

cd "$ORIGINALPATH"

for i in `seq 1 4` ; do
    # Echo "Press Enter to start" #add this back if using --pause
    INSTANSTIME=$(eval "$INSTANS_HOME/bin/instans --prefix-encoding=true --print-prefix-encodings=false --select-output-csv=/dev/null $INSTANSQUERIES -i \"$IP$1.ttl\" --allow-rule-instance-removal=true --rdf-operations=add:execute:remove:flush --time=- --input-blocks=\"$SDAT$2.trig\" -i ../instans/endMarker.ttl" | grep Done | sed 's/[^0-9+\.0-9+]//g')
    Echo "INSTANS,$1,$2,$i,$INSTANSTIME"
done
