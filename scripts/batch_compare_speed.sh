# Experiment 1:
./compare_speed_comm_pack.sh parameters_exp1 eventRun_100_exp1
./compare_speed_comm_pack.sh parameters_exp1 eventRun_1000_exp1
./compare_speed_comm_pack.sh parameters_exp1 eventRun_10000_exp1
./compare_speed_comm_pack.sh parameters_exp1 eventRun_100000_exp1
# ./compare_speed_comm_pack.sh parameters_exp1 eventRun_200000_exp1
./compare_speed_comm_pack.sh parameters_exp1 eventRun_1000000_exp1

# Experiment 2:
./compare_speed_comm_pack.sh parameters_exp2 eventRun_10000_exp2_1
./compare_speed_comm_pack.sh parameters_exp2 eventRun_10000_exp2_2
./compare_speed_comm_pack.sh parameters_exp2 eventRun_10000_exp2_3

# Experiment 3:
./compare_speed_comm_pack.sh parameters_exp3 eventRun_100000_exp3_run1
./compare_speed_comm_pack.sh parameters_exp3 eventRun_100000_exp3_run2
./compare_speed_comm_pack.sh parameters_exp3 eventRun_100000_exp3_run3
./compare_speed_comm_pack.sh parameters_exp3 eventRun_100000_exp3_run4

# Experiment 4:
./compare_speed_comm_pack.sh parameters_exp4 eventRun_1000_exp4
./compare_speed_comm_pack.sh parameters_exp4 eventRun_10000_exp4
./compare_speed_comm_pack.sh parameters_exp4 eventRun_100000_exp4
# Crashes the heap on INSTANS 16.6.2015
# Did not finish between 2.7.-9.7.2015 on an increased heap size
# ./compare_speed_comm_pack.sh parameters_exp4 eventRun_1000000_exp4

# Experiment 5:
./compare_speed_comm_pack_ship.sh parameters_exp5 eventRun_100_exp5
./compare_speed_comm_pack_ship.sh parameters_exp5 eventRun_1000_exp5
./compare_speed_comm_pack_ship.sh parameters_exp5 eventRun_10000_exp5
./compare_speed_comm_pack_ship.sh parameters_exp5 eventRun_100000_exp5
# Crashes on INSTANS 11.6.2015
# Crashed with a token collision 2.7.2015
# ./compare_speed_comm_pack_ship.sh parameters_exp5 eventRun_1000000_exp5

